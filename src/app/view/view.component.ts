import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
import {NgbDate, NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
 import { DatepickerServiceInputs} from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  districtList: any = ['द.कोल्हापूर', 'उ. कोल्हापूर', 'सांगली', '	कऱ्हाड', 'सातारा','पुणे','पिंपरी चिंचवड','बारामती','पंढरपूर','सोलापूर','द. नगर','उ. नगर','नाशिक शहर','नाशिक ग्रामीण','मालेगाव','कसबा','	संभाजी','सिंहगड','पर्वती','कात्रज','हडपसर','वडगाव शेरी','	येरवडा','विद्यापीठ'];
   tablePranali :any =[
     {name: 'Name1', district:'Pune',class:'First',use:'personal',date:'12/07/2021'},
    {name: 'Name2', district:'second', use:'personal use'} 
  ] 


  form = new FormGroup({
    district : new FormControl()
    
  })

constructor(){}

  

  ngOnInit(): void {
  }
}