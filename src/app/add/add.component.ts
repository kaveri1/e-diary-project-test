import { Component, OnInit } from '@angular/core';
import { FormArrayName, FormControl, FormGroup,FormArray,FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common'
import { ActivatedRoute, Router,Params } from '@angular/router';
import * as uuid from 'uuid';



@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  districtList: any = ['द.कोल्हापूर', 'उ. कोल्हापूर', 'सांगली', '	कऱ्हाड', 'सातारा','पुणे','पिंपरी चिंचवड','बारामती','पंढरपूर','सोलापूर','द. नगर','उ. नगर','नाशिक शहर','नाशिक ग्रामीण','मालेगाव','कसबा','	संभाजी','सिंहगड','पर्वती','कात्रज','हडपसर','वडगाव शेरी','	येरवडा','विद्यापीठ'];

  displaydistrictList: any = ['All','द.कोल्हापूर', 'उ. कोल्हापूर', 'सांगली', '	कऱ्हाड', 'सातारा','पुणे','पिंपरी चिंचवड','बारामती','पंढरपूर','सोलापूर','द. नगर','उ. नगर','नाशिक शहर','नाशिक ग्रामीण','मालेगाव','कसबा','	संभाजी','सिंहगड','पर्वती','कात्रज','हडपसर','वडगाव शेरी','	येरवडा','विद्यापीठ'];

  vargList: any =['प्राथमिक','प्रथम','द्वितीय','तृतीय','वर्ग नाही'];
  user={district:'',varg:'',name:'',use:'',date:null};
  userlist:any=[];
  addformdisplay=true;
  tablePranali :any =[
     {name: 'Name1', class:'first', use:'office use'},
    {name: 'Name2', class:'second', use:'personal use'} 
  ]
  startDate:any=null;
  endDate:any=null;
  date:any=null;
  latest_date:any=null;
  filter=false;
  filterlist:any=[];
  filterdistrict:any='';
  searchtext:any='';
  inputbox=false;

  form = new FormGroup({
    district : new FormControl()
    
  })
  constructor(public datepipe: DatePipe,
    private route: ActivatedRoute) { }
  ngOnInit(): void {
   
  }
   ngOnChange(){
   }
  

  

  submit(user:any){
    this.myFunction();
   user._id = uuid.v4();
   this.userlist.push(JSON.parse(JSON.stringify(this.user)));
   console.log(this.userlist);
   this.addRow();
   alert("आपण केलेली नोंद यशस्वीरीत्या समाविष्ट करण्यात आली आहे") 
  }

  
  myFunction(){
    this.date=new Date();
    this.latest_date =this.datepipe.transform(this.date, 'yyyy-MM-dd');
    this.user.date= this.latest_date;
   }


  
  onstartDatefilter(date:any){
    let sdate:any
    this.startDate =this.datepipe.transform(date, 'yyyy-MM-dd')
    this.filterlist=[];
    for(let i=0;i<this.userlist.length;i++){
      sdate={ $gte: this.startDate };
      if(this.userlist[i].date>=sdate.$gte){
        this.filterlist.push(JSON.parse(JSON.stringify(this.userlist[i])));
      }
    }
    this.filter=true;
  }
  onendDatefilter(date:any){
    let edate:any
    this.endDate =this.datepipe.transform(date, 'yyyy-MM-dd')
    this.filterlist=[];
    for(let i=0;i<this.userlist.length;i++){
      edate={ $gte: this.endDate };
      if(this.userlist[i].date<=edate.$gte){
        this.filterlist.push(JSON.parse(JSON.stringify(this.userlist[i])));
      }
    }
    this.filter=true;
  }
  search(){
    if(this.inputbox==false){
      this.inputbox=true;
    }else{
      this.inputbox=false;
    }
    this.filterlist=[];
    for(let i=0;i<this.userlist.length;i++){
      if(this.userlist[i].name==this.searchtext ||this.userlist[i].district==this.searchtext ||this.userlist[i].varg==this.searchtext ||this.userlist[i].use==this.searchtext){
        this.filterlist.push(JSON.parse(JSON.stringify(this.userlist[i])));
  }
  this.filter=true;
  
}

}
  ondistrictSelectedfilter(district:any){
    if(district!='All'){
    this.filterlist=[];
    for(let i=0;i<this.userlist.length;i++){
      if(this.userlist[i].district==district){
        this.filterlist.push(JSON.parse(JSON.stringify(this.userlist[i])));
        this.filter=true;
      }
    }
  }else{
    this.filter=false;
  }
}
  onageSelected(varg:any){
    this.user.varg=varg;
  }
  ondistrictSelected(district:any){
    this.user.district=district;
  }
  addform(){
    this.addformdisplay=true;
  }
  showform(){
    this.addformdisplay=false;
  }
  openUserEdit(data:any,i:any){
    this.addformdisplay=true;
    this.user=data;
    this.openUserRemoveModale(i)
  }
  share(){

  }

  openUserRemoveModale(data:any){
    let j = this.userlist.findIndex((x:any) => x._id === data);
    this.userlist.splice(j, 1);
  }

  addRow() {
    this.user={district:'',varg:'',name:'',use:'',date:null};
  }
  
}